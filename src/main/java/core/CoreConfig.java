package core;

import org.apache.commons.lang3.BooleanUtils;

import com.alibaba.druid.filter.stat.StatFilter;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.druid.DruidStatViewHandler;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.jfinal.render.FreeMarkerRender;
import com.jfinal.render.ViewType;
import com.jfinal.template.Engine;

import cn.dreampie.shiro.freemarker.ShiroTags;
import plugin.route.AutoBindRoutes;
import plugin.shiro.ShiroInterceptor;
import plugin.shiro.ShiroPlugin;
import web.handler.SessionHandler;
import web.interceptor.MessageInterceptor;
import web.model._MappingKit;

public class CoreConfig extends JFinalConfig {
	@Override
	public void configConstant(Constants me) {
		loadPropertyFile("db.properties");
		me.setDevMode(BooleanUtils.toBoolean(getProperty("DEV_MODE")));
		FreeMarkerRender.setProperty("template_update_delay", "0");
		// 模板更更新时间,0表示每次都加载
		FreeMarkerRender.setProperty("classic_compatible", "true");
		// 如果为null则转为空值,不需要再用!处理
		FreeMarkerRender.setProperty("whitespace_stripping", "true");
		FreeMarkerRender.setProperty("auto_import", "/WEB-INF/templates/layout/taglib.ftl as lib");
		// 去除首尾多余空格
		// 设定错误页面
		me.setViewType(ViewType.FREE_MARKER);
		me.setViewExtension("ftl");
		me.setError404View("/404.html");
		me.setError500View("/500.html");
		me.setErrorView(401, "/au/login.html");
		me.setErrorView(403, "/au/login.html");
		me.setBaseUploadPath(Const.UPLOAD_REAL_PATH);
		me.setBaseDownloadPath(Const.UPLOAD_REAL_PATH);
	}

	@Override
	public void configRoute(Routes me) {
		// me.add("/", IndexController.class);
		// me.add("/user", UserController.class);
		// 使用注解
		// me.add("/user", UserController.class);
		me.add(new AutoBindRoutes());

	}
	@Override
	public void configPlugin(Plugins me) {
		DruidPlugin druid = null;
		try {
			// druid ConfigTools.decrypt()密码加密
			druid = new DruidPlugin(getProperty("JDBC_URL"), getProperty("USER_NAME"), getProperty("PASSWORD"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		druid.setInitialSize(3).setMaxActive(10);
		druid.addFilter(new StatFilter());
		druid.setValidationQuery("select  1");
		me.add(druid);
		ActiveRecordPlugin arp = new ActiveRecordPlugin(druid);
		_MappingKit.mapping(arp);
		arp.setShowSql(BooleanUtils.toBoolean(getProperty("SHOW_SQL")));
		me.add(arp);
		me.add(new EhCachePlugin());
		// shiro插件
		ShiroPlugin shiroPlugin = new ShiroPlugin(Routes.getRoutesList());
		shiroPlugin.setLoginUrl("/admin/login");
		shiroPlugin.setSuccessUrl("/admin/list");
		shiroPlugin.setUnauthorizedUrl("/admin/login");
		me.add(shiroPlugin);
	}

	@Override
	public void configInterceptor(Interceptors me) {
		me.add(new ShiroInterceptor());
		me.add(new MessageInterceptor());
	}

	@Override
	public void configHandler(Handlers me) {

		// Druid链接池状态处理器，可以通过访问 http://ip:port/druid 来查看数据库访问情况。
		DruidStatViewHandler dvh = new DruidStatViewHandler("/druid");
		me.add(dvh);
		ContextPathHandler path = new ContextPathHandler("ctx");
		me.add(path);
		me.add(new SessionHandler());
	}

	@Override
	public void afterJFinalStart() {
		FreeMarkerRender.getConfiguration().setSharedVariable("shiro", new ShiroTags());
	}

	@Override
	public void beforeJFinalStop() {
	}

	@Override
	public void configEngine(Engine me) {
		// TODO Auto-generated method stub

	}
}