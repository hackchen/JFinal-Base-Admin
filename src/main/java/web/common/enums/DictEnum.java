package web.common.enums;

public enum DictEnum {
	DEL_FLAG("del_flag"), MESSAGE_TYPE("message_type"), BYE_TYPE("bye_type"), CALL_REASON("call_reason");

	private String str;

	private DictEnum(String str) {
		this.str = str;
	}

	public String getConst() {
		return str;
	}

}
