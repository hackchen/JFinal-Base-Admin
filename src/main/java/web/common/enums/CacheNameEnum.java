package web.common.enums;

public enum CacheNameEnum {
	WECHAT("wechat_"), USER("user_"), MIQ_USER("miq_user_"), GIRL("girl_"), USERACCOUNT("user_account_"), DICT(
			"dict_"), WECHATMESSAGE(
			"wechatmessage_"), WECHAT_GIRL(
					"wechat_girl_"), CHAT_TOPIC("chat_topic_"), REPLAY_KEYWORD("replay_keyword_");

	private String name; 
	private CacheNameEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	/**
	* @Title: getIdKey
	* @Description: id为key的缓存
	* @author yangyw
	* @param id
	* @return
	* @throws
	*/
	public String getIdKey(long id) {
		return this.name + ConstEnum.ID_KEY.getConst() + id;
	}

	/**
	* @Title: getUserKey
	* @Description: 仅限userid做key的
	* @author yangyw
	* @param userid
	* @return
	* @throws
	*/
	public String getUserKey(long userid) {
		return this.name + ConstEnum.USER_ID_KEY.getConst() + userid;
	}
	/**
	* @Title: getKey
	* @Description: 任意key
	* @author yangyw
	* @param anyother
	* @return
	* @throws
	*/
	public String getKey(String anyother) {
		return this.name + ConstEnum.OTHER_KEY.getConst() + anyother;
	}
	public String getListName() {
		return name + ConstEnum.LIST_KEY.getConst();
	}

	public String getMapName() {
		return name + ConstEnum.MAP_KEY.getConst();
	}
}
