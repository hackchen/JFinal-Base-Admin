package web.validator;

import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;

import web.model.temp.WaitPage;

public class AddUserValidator extends Validator {

	@Override
	protected void validate(Controller c) {
		validateRequired("username", "namemsg", "请输入用户名");
		validateRequired("password", "passwordmsg", "请输入密码");
	}

	@Override
	protected void handleError(Controller c) {
		WaitPage waitPage = new WaitPage();
		waitPage.setMsg("输入信息有误");
		waitPage.setStatus("danger");
		waitPage.setUrl(c.getRequest().getContextPath() + "/user/list");
		c.setAttr("waitPage", waitPage);
		c.render("/WEB-INF/templates/layout/inc/submit.ftl");
	}

}
