package web.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.log.Log;

import kit.IP;
import web.model.AdminLoginLog;

public class AdminLoginLogInterceptor implements Interceptor {
	private static final Log log = Log.getLog(AdminLoginLogInterceptor.class);

	@Override
	public void intercept(Invocation inv) {
		Controller controller = inv.getController();
		String username = controller.getPara("username");
		if (kit.StringUtil.isNotBlank(username)) {
			AdminLoginLog loginlog = new AdminLoginLog();
			loginlog.setUsername(username);
			loginlog.setIp(IP.getIpAddr(controller.getRequest()));
			loginlog.save();
			inv.invoke();
		} else {
			controller.redirect("/admin/login");
		}
	}

}
