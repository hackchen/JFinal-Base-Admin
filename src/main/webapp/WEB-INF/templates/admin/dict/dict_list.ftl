<!DOCTYPE html>
<html>
<head>
<@lib.adminhead/>

<script type="text/javascript">
	
	function oper_list(){
		form1.action = '${ctx}/admin/dict/list';
		form1.submit();
	}
	
	function oper_view(pid){
	     window.location.href='${ctx}/admin/dict/view/'+pid;
	}
	
	function oper_add(){
		window.location.href='${ctx}/admin/dict/add';
	}
	
	function oper_edit(pid){
		window.location.href= '${ctx}/admin/dict/edit/'+pid;
	}
	
	function oper_del(pid){
		layer.confirm('确认删除字典表信息?', {
		    btn: ['确定','取消'] 
		}, function(){
		    layer.msg('正在删除中..', {icon: 1});
		    window.location.href="${ctx}/admin/dict/delete/"+pid;
		});
		
		
	}
	
	</script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">字典表列表</h1>
			</div>
		</div>
		<div class="row">
			<form class="form-inline" name="form1" action="" method="post" id="searchform">
				<input type="hidden" name="page" id="pageindex">
				<div class="col-lg-12 form-group">
					<div class="form-group">
						<input class="form-control" type="text" name="attr.value" value="${attr.value}" placeholder="请输入数据值" />
					</div>
					<div class="form-group">
						<input class="form-control" type="text" name="attr.label" value="${attr.label}" placeholder="请输入标签名" />
					</div>
					<div class="form-group">
						<input class="form-control" type="text" name="attr.type" value="${attr.type}" placeholder="请输入类型" />
					</div>
					<button type="button" class="btn btn-default" onclick="oper_list();" name="search">查 询</button>
					<button type="button" class="btn btn-default" onclick="oper_add();">新 增</button>
				</div>
			</form>
		</div>
		<hr />
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">字典表列表</div>
					<!-- /.panel-heading -->
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th>#</th>
										<th>数据值</th>
										<th>标签名</th>
										<th>类型</th>
										<th>排序（升序）</th>
										<th>备注信息</th>
										<th>删除标记</th>
										<th>操作</th>
									</tr>
								</thead>
								<tbody>
									<#if page.list!> <#list page.list as item>
									<tr>
										<td>${item.id}</td>
										<td>${item.value}</td>
										<td>${item.label}</td>
										<td>${item.type}</td>
										<td>${item.sort}</td>
										<td>${item.remarks}</td>
										<td>
										<#if item.del_flag==0>
											<button class="btn btn-success status" data-id="${item.id}"> ${ item.delflagstr}</button>
										<#else>
											<button class="btn btn-danger status" data-id="${item.id}">${ item.delflagstr}</button>
										</#if>
										
										</td>
										<td>
										<a href="javascript:void(0);" class="btn-sm btn btn-info" onclick="oper_view(${item.id});">查看</a>
										<a href="javascript:void(0);" class="btn-sm btn btn-warning" onclick="oper_edit(${item.id});">修改</a>
										<a href="javascript:void(0);" class="btn btn-sm btn-danger" onclick="oper_del(${item.id});">删除</a></td>
									</tr>
									</#list> <#else>
									<tr>
										<td colspan="8">没有记录</td>
									</tr>
									</#if>
								</tbody>
							</table>
						</div>
						<@lib.searchpage page/>
					</div>
				</div>
			</div>
		</div>
	</div>
	<@lib.adminfootjs/>
	<script type="text/javascript">
		function page(page){
			$("#pageindex").val(page);
			form1.action = '${ctx}/admin/dict/list';
			form1.submit();
		}
		$(function(){
			$(".status").click(function(){
				var btn = $(this);
				var id =btn.data("id");
				$.getJSON("${ctx}/admin/dict/status/"+id,  function(jsonData){
					  	if(jsonData.code==200){
					  		if(jsonData.data.del_flag==0){
					  			btn.removeClass("btn-danger");
					  			btn.addClass("btn-success");
					  			btn.html("显示");
					  		}else{
					  			btn.removeClass("btn-success");
					  			btn.addClass("btn-danger");
					  			btn.html("隐藏");
					  		}
					  	}
				 });
			});
		});
	</script>
</body>
</html>