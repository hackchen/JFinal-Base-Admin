<!DOCTYPE html>
<html>
<head>
<@lib.adminhead/>
</head>
<body>
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">查看字典表
								<a href="${ctx}/admin/dict/list" class="btn btn-default">列表</a>
                            	<a href="${ctx}/admin/dict/add"  class="btn btn-default">新增</a>
                            	<a href="${ctx}/admin/dict/edit/${model.id}"  class="btn btn-default">修改</a>
						</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
                       		 <div class="panel-body">
                       		 			<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
                       		 <div class="panel-body">
                           	 	<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover">
										 										<tr>
										 <td class="col-sm-4 text-center ">数据值:</td>
											<td class="col-sm-8 text-left">
													${model.value}
</td>											</tr>
										 										<tr>
										 <td class="col-sm-4 text-center ">标签名:</td>
											<td class="col-sm-8 text-left">
													${model.label}
</td>											</tr>
										 										<tr>
										 <td class="col-sm-4 text-center ">类型:</td>
											<td class="col-sm-8 text-left">
													${model.type}
</td>											</tr>
										 										<tr>
										 <td class="col-sm-4 text-center ">排序（升序）:</td>
											<td class="col-sm-8 text-left">
													${model.sort}
</td>											</tr>
										 										<tr>
										 <td class="col-sm-4 text-center ">备注信息:</td>
											<td class="col-sm-8 text-left">
													${model.remarks}
</td>											</tr>
										 										<tr>
										 <td class="col-sm-4 text-center ">删除标记:</td>
											<td class="col-sm-8 text-left">
													${model.del_flag}
</td>											</tr>
										 										<tr>
										 <td class="col-sm-4 text-center ">创建时间:</td>
											<td class="col-sm-8 text-left">
													${model.create_time}
</td>											</tr>
										 										<tr>
										 <td class="col-sm-4 text-center ">更新时间:</td>
											<td class="col-sm-8 text-left">
													${model.modify_time}
</td>											</tr>
									</table>
	                           	 </div>
                        	</div>
						</div>
					</div>
				</div>
			</div>
		<@lib.adminfootjs/>

</body>
