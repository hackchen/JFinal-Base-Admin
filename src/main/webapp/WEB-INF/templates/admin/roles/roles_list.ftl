<!DOCTYPE html>
<html>
<head>
<@lib.adminhead />
<@lib.adminfootjs />
 <@lib.ztree />
<script type="text/javascript">
	
	
	function oper_add(){
		$.get("${ctx}/admin/roles/add", function(data){
			   $("#doit").html(data); //返回的data是字符串类型
			},"html");
	}
	
	function oper_edit(pid){
		$.get('${ctx}/admin/roles/edit/'+pid, function(data){
			   $("#doit").html(data); //返回的data是字符串类型
			},"html");
	}
	function oper_perm(id){
		$.get('${ctx}/admin/roles/perm/'+id, function(data){
			   $("#doit").html(data); //返回的data是字符串类型
			},"html");
	}
	
	function oper_del(pid){
		layer.confirm('确认删除角色配置信息?', {
		    btn: ['确定','取消'] 
		}, function(){
		    layer.msg('正在删除中..', {icon: 1});
		    window.location.href="${ctx}/admin/roles/delete/"+pid;
		});
	}
	
	</script>
</head>
<body>
	<div id="wrapper">

			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">角色配置列表</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">角色配置列表 
								<@shiro.hasPermission name="admin:roles:add">
									<button type="button" class="btn btn-default" onclick="oper_add();">新 增</button>
								</@shiro.hasPermission>
							</div>
							<!-- /.panel-heading -->
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>#</th>
												<th>角色名称</th>
												<th>描述</th>
												<th>操作</th>
											</tr>
										</thead>
										<tbody>
											<#if page.list!> <#list page.list as item>
											<tr>
												<td>${item.id}</td>
												<td>${item.name}</td>

												<td>${item.remark}</td>

												<td>
												<@shiro.hasPermission name="admin:roles:edit">
												<a href="javascript:void(0);" class="btn-sm btn btn-warning" onclick="oper_edit(${item.id});">修改</a>
												</@shiro.hasPermission>
												<@shiro.hasPermission name="admin:roles:del">
													<a href="javascript:void(0);" class="btn btn-sm btn-danger" onclick="oper_del(${item.id});">删除</a>
													</@shiro.hasPermission>
														<@shiro.hasPermission name="admin:roles:perm">
													<a href="javascript:void(0);" class="btn btn-sm btn-primary" onclick="oper_perm(${item.id});">权限</a>
													</@shiro.hasPermission>
												</td>
											</tr>
											</#list> <#else>
											<tr>
												<td colspan="8">没有记录</td>
											</tr>
											</#if>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">操作面板 </div>
							<div class="panel-body" id="doit">
							
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


	<script type="text/javascript">
		function page(page){
			$("#pageindex").val(page);
			form1.action = '${ctx}/admin/roles/list';

			form1.submit();
		}
	</script>
</body>
</html>