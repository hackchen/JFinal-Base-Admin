<!DOCTYPE html>
<html>
<head>
<@lib.adminhead /> <@lib.adminfootjs /> <#include
"WEB-INF/templates/layout/inc/ztree.ftl"/>

<SCRIPT type="text/javascript">
	var setting = {
		view : {
			addDiyDom: addDiyDom,
			showIcon : false
		},
		data : {
			simpleData : {
				enable : true
			}
		},
		callback: {
			  		onExpand: cookieSave,
			  		onCollapse: cookieRemove
		}
		
		
	};
	var IDMark_A = "_a";
	var zNodes = ${znodes};

	function showIconForTree(treeId, treeNode) {
		return !treeNode.isParent;
	};
	function addDiyDom(treeId, treeNode) {
		if (treeNode.parentNode && treeNode.parentNode.id!=2) return;
		var aObj = $("#" + treeNode.tId + IDMark_A);
		var editStr ="";
		<@shiro.hasPermission name="admin:perm:add">
		editStr =editStr+ "<a id='addchild_" +treeNode.id+ "' style='color: green;' onclick='addChild(\""+treeNode.id+"\");return false;'>添加下级</a>";
		 </@shiro.hasPermission>
		<@shiro.hasPermission name="admin:perm:edit">
			editStr =editStr+"<a id='modify_" +treeNode.id+ "' style='color: blue;' onclick='modify(\""+treeNode.id+"\");return false;'>修改</a>";
		 </@shiro.hasPermission>
		if(!treeNode.isParent){
			<@shiro.hasPermission name="admin:perm:del">
				editStr =editStr+"<a id='deltree_" +treeNode.id+ "' style='color: red;' onclick='oper_del(\""+treeNode.id+"\");return false;'>删除</a>";
			</@shiro.hasPermission>
		}
		aObj.after(editStr);
	}
	$(document).ready(function() {
		$.fn.zTree.init($("#treeDemo"), setting, zNodes);
		initExpand("treeDemo");
	});
	function addChild(pid){
		$.get("${ctx}/admin/permissions/add", {"pid":pid}, function(data){
		   $("#doit").html(data); //返回的data是字符串类型
		},"html");
	}
	function modify(id){
		$.get("${ctx}/admin/permissions/edit/"+id, function(data){
		   $("#doit").html(data); //返回的data是字符串类型
		},"html");
	}
	function oper_del(pid){
		layer.confirm('确认删除权限配置信息?', {
		    btn: ['确定','取消'] 
		}, function(){
		    layer.msg('正在删除中..', {icon: 1});
		    window.location.href="${ctx}/admin/permissions/delete/"+pid;
		});
	}
</SCRIPT>
</head>
<body>
	<div id="wrapper">

			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">权限配置列表</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">显示权限 
						<@shiro.hasPermission name="admin:perm:add">
							<button type="button" class="btn btn-success" onclick="addChild('0');">增加</button>
						 </@shiro.hasPermission>
							
							</div>
							<!-- /.panel-heading -->
							<div class="panel-body">
								<div class="left">
									<ul id="treeDemo" class="ztree"></ul>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">操作面板</div>
							<!-- /.panel-heading -->
							<div class="panel-body" id="doit">
								


							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

</body>
</html>